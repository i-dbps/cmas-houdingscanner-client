import time

from constants import *
from .interface import *

from .start_interface import StartInterface

from PySide2.QtWidgets import QPushButton, QVBoxLayout

class BootInterface(Interface):
    
    def __init__(self, main):
        super().__init__(main)

        self.start_time = time.time()

        widget = QWidget()
        layout = QVBoxLayout()

        widget.setObjectName('boot_background')
        widget.setStyleSheet(open('../res/css/boot.css', 'r').read())

        layout.addWidget(widget)
        self.setLayout(layout)

    def update(self):
        if time.time() - self.start_time > 2:
            self.main.set_interface(StartInterface(self.main))