from constants import *
from .interface import *

from .id_interface import IdInterface
from .exercise_overview_interface import ExerciseOverviewInterface

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QLabel, QPushButton, QFrame, QVBoxLayout, QHBoxLayout

class StartInterface(Interface):

	def __init__(self, main):
		super().__init__(main)

		self.setStyleSheet(open('../res/css/start.css', 'r').read())

		layout = QVBoxLayout()

		if main.identification.is_identified():
			label = QLabel(text="Klik op 'Oefeningen' om te beginnen")
			button = QPushButton('Oefeningen')
			hbox = QHBoxLayout()

			button.setMaximumWidth(600)
			self.add_shadow(button)

			def on_click():
				main.set_interface(ExerciseOverviewInterface(main))

			button.clicked.connect(on_click)

			hbox.addWidget(button)

			layout.addWidget(label)
			layout.addLayout(hbox)
			layout.setAlignment(Qt.AlignVCenter | Qt.AlignCenter)
		else:
			button = QPushButton('Identificeren')

			button.setMaximumWidth(600)
			self.add_shadow(button)

			def on_click():
				main.set_interface(IdInterface(main))

			button.clicked.connect(on_click)

			layout.addWidget(button)
			layout.setAlignment(Qt.AlignCenter)

		self.setLayout(layout)