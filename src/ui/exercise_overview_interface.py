from constants import *
from .interface import *

from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QLabel, QPushButton, QFrame, QVBoxLayout, QHBoxLayout
from PySide2.QtGui import QIcon, QPixmap, QPainter

class ExerciseOverviewInterface(Interface):

    def __init__(self, main):
        super().__init__(main)

        self.setStyleSheet(open('../res/css/overview.css', 'r').read())

        layout = QVBoxLayout()

        label = QLabel(text='Klik op een oefening')
        label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        layout.addWidget(label)

        i = 0
        for j in range(4):
            row = QHBoxLayout()

            for k in range(4):
                if i == 14:
                    button = QPushButton(text='Terug')
                    button.setObjectName('back_button')

                    def on_click():
                        from .start_interface import StartInterface
                        main.set_interface(StartInterface(main))

                    button.clicked.connect(on_click)

                    self.add_shadow(button)
                    row.addWidget(button)
                elif i == 15:
                    button = QPushButton(text='Verzenden')

                    completed = main.scan.is_completed()
                    if completed:
                        button.setObjectName('send_button')

                        def on_click():
                            from .start_interface import StartInterface
                            main.scan.export()
                            main.set_interface(StartInterface(main))

                        button.clicked.connect(on_click)
                    else:
                        button.setObjectName('back_button')
                    button.setEnabled(completed)

                    self.add_shadow(button)
                    row.addWidget(button)
                elif i < len(main.scan.exercises):
                    button = QPushButton()
                    button.setIcon(self.create_icon(main.scan.exercises[i]))
                    button.setIconSize(QSize(180, 180))

                    def on_click(index):
                        def go():
                            exercise = main.scan.exercises[index]
                            if not exercise.completed:
                                from .exercise_options_interface import ExerciseOptionsInterface
                                main.set_interface(ExerciseOptionsInterface(main, exercise))
                        return go

                    button.clicked.connect(on_click(i))

                    row.addWidget(button)

                i += 1

            widget = QWidget()
            widget.setLayout(row)
            layout.addWidget(widget)

        self.setLayout(layout)

    def create_icon(self, exercise):
        image = QPixmap(Path.RES + exercise.pose.get_image_name())

        if exercise.completed:
            checkmark = QPixmap(Path.RES + 'checkmark.png')
            checkmark = checkmark.scaled(250, 250)
 
            painter = QPainter(image)
            painter.drawPixmap(0, 0, checkmark)
            painter.end()
 
        icon = QIcon()
        icon.addPixmap(image)
        return icon