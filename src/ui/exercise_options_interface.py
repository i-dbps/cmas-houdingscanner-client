from .interface import *

from .exercise_overview_interface import ExerciseOverviewInterface
from .exercise_interface import ExerciseInterface

from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QLabel, QPushButton, QFrame, QVBoxLayout, QHBoxLayout
from PySide2.QtGui import QImage, QPixmap

class ExerciseOptionsInterface(Interface):

	def __init__(self, main, exercise):
		super().__init__(main)

		self.setStyleSheet(open('../res/css/options.css', 'r').read())

		layout = QVBoxLayout()
		middle_layout = QHBoxLayout()
		bottom_layout = QHBoxLayout()

		label =	QLabel(text=exercise.pose.get_name())
		info_button = QPushButton(text='Info')
		start_button = QPushButton(text='Start')
		back_button = QPushButton(text='Terug')

		label.setObjectName('title_label')
		label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)

		info_button.setObjectName('info_button')
		start_button.setObjectName('start_button')
		back_button.setObjectName('back_button')

		layout.addWidget(label)

		middle_layout.addWidget(info_button)
		middle_layout.addWidget(start_button)
		layout.addLayout(middle_layout)

		bottom_layout.addWidget(back_button)
		layout.addLayout(bottom_layout)

		def on_click_info():
			pass

		def on_click_start():
			main.set_interface(ExerciseInterface(main, exercise))

		def on_click_back():
			main.set_interface(ExerciseOverviewInterface(main))

		info_button.clicked.connect(on_click_info)
		start_button.clicked.connect(on_click_start)
		back_button.clicked.connect(on_click_back)

		self.add_shadow(info_button)
		self.add_shadow(start_button)
		self.add_shadow(back_button)

		self.setLayout(layout)