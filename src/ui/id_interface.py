from constants import *
from .interface import *

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QPushButton, QFrame, QLineEdit, QVBoxLayout, QLabel, QHBoxLayout, QWidget

class IdInterface(Interface):
    
    def __init__(self, main):
        super().__init__(main)

        self.setStyleSheet(open('../res/css/id.css', 'r').read())

        button_ok = QPushButton(text="Ok")
        button_back = QPushButton(text="Terug")
        label = QLabel(text="Voer een zescijferig indetificatie code in.")
        textbox = QLineEdit()

        layout = QVBoxLayout()
        textbox_layout = QHBoxLayout()
        button_layout = QHBoxLayout()

        widget = QWidget()
        widget_layout = QVBoxLayout()
        
        self.add_shadow(button_ok)
        self.add_shadow(button_back)

        def on_click_ok():
            if len(textbox.text()) > 0:
                from .start_interface import StartInterface

                main.identification.id = textbox.text()
                main.identification.save()

                main.set_interface(StartInterface(main))

        button_ok.clicked.connect(on_click_ok)

        def on_click_back():
            from .start_interface import StartInterface
            main.set_interface(StartInterface(main))

        button_back.clicked.connect(on_click_back)

        label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        layout.addWidget(label)

        textbox_layout.addWidget(textbox)

        button_layout.addWidget(button_back)
        button_layout.addWidget(button_ok)

        widget_layout.addLayout(textbox_layout)
        widget_layout.addLayout(button_layout)
        widget.setLayout(widget_layout)
        layout.addWidget(widget)

        self.setLayout(layout)
