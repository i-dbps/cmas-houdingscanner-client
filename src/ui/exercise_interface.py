import cv2
import time

from .interface import *

from scan.camera import Camera
from scan.wireframe import Wireframe

from .exercise_overview_interface import ExerciseOverviewInterface

from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QLabel, QPushButton, QFrame, QVBoxLayout, QHBoxLayout
from PySide2.QtGui import QImage, QPixmap

class ExerciseInterface(Interface):

	def __init__(self, main, exercise):
		super().__init__(main)
		self.exercise = exercise

		self.setStyleSheet(open('../res/css/exercise.css', 'r').read())

		layout = QVBoxLayout()
		middle_layout = QHBoxLayout()
		bottom_layout = QHBoxLayout()

		image_label = QLabel()
		title_label = QLabel(text=exercise.pose.get_name())
		self.status_label = QLabel(text='Geen houding gevonden')
		self.camera_label = QLabel()

		button = QPushButton(text='Terug')
		button.setObjectName('back_button')

		image_label.setObjectName('image_label')
		self.camera_label.setObjectName('image_label')

		title_label.setObjectName('title_label')
		title_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)

		self.status_label.setObjectName('status_label')
		self.status_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
		layout.addWidget(title_label)

		image_label.setPixmap(QPixmap(Path.RES + exercise.pose.get_image_name()))
		middle_layout.addWidget(self.camera_label)
		middle_layout.addWidget(image_label)
		layout.addLayout(middle_layout)

		bottom_layout.addWidget(button)
		bottom_layout.addWidget(self.status_label)
		layout.addLayout(bottom_layout)

		def on_click():
			main.set_interface(ExerciseOverviewInterface(main))

		button.clicked.connect(on_click)
		self.add_shadow(button)

		self.setLayout(layout)

		self.camera = Camera()
		self.wireframe = Wireframe()
		self.wireframe.start(self.camera)

		self.start_time = -1

	def update(self):
		if not self.exercise.completed:
			if self.exercise.pose.is_in_pose(self.wireframe):
				if self.start_time == -1:
					self.start_time = time.time()

				self.status_label.setText('Tijd: ' + str(int(time.time() - self.start_time)))
			elif self.start_time != -1:
				self.exercise.completed = True

				end_time = time.time() - self.start_time
				self.exercise.score = self.exercise.pose.calculate_score(end_time)
				self.exercise.time = end_time

				self.status_label.setText('Oefening klaar, score: ' + str(self.exercise.score))

		result, camera_read = self.camera.read_frame()

		if result:
			self.wireframe.debug(camera_read)
			frame = cv2.cvtColor(camera_read, cv2.COLOR_BGR2RGB)

			height, width, channels = frame.shape
			image = QImage(frame.data, width, height, channels * width, QImage.Format_RGB888)
			self.camera_label.setPixmap(QPixmap(image))

	def dispose(self):
		try:
			self.wireframe.stop()
			self.camera.dispose()
		except:
			pass