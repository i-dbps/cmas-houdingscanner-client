from constants import *

from PySide2.QtWidgets import QWidget, QGraphicsDropShadowEffect

class Interface(QWidget):

	def __init__(self, main):
		super().__init__(main)

		self.main = main

	def update(self):
		pass

	def render(self, screen):
		pass

	def dispose(self):
		pass

	def add_shadow(self, widget):
		shadow = QGraphicsDropShadowEffect()
		shadow.setBlurRadius(50)
		shadow.setYOffset(3.5)
		shadow.setXOffset(-2.0)
		widget.setGraphicsEffect(shadow)
		widget.graphicsEffect()