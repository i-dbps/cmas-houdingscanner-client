class PointId:
	HEAD = 0
	FACE = 1
	RIGHT_SHOULDER, RIGHT_ELBOW, RIGHT_HAND = 2, 3, 4
	LEFT_SHOULDER, LEFT_ELBOW, LEFT_HAND = 5, 6, 7
	BELLY = 14
	RIGHT_HIP, RIGHT_KNEE, RIGHT_FOOT = 8, 9, 10
	LEFT_HIP, LEFT_KNEE, LEFT_FOOT = 11, 12, 13

	LINKS = [[0,1], [1,2], [2,3], [3,4], [1,5], [5,6], [6,7], [1,14], [14,8], [8,9], [9,10], [14,11], [11,12], [12,13]]

	POINT_COUNT = 15

class UI:
	TITLE = 'CMas'
	WIDTH = 1920
	HEIGHT = 1080

	DEFAULT_FONT = 'Comic Sans MS'

class Path:
	RES = '../res/'
	PROTO_FILE = RES + 'proto.prototxt'
	WEIGHTS_FILE = RES + 'weights.caffemodel'

	ID_FILE = RES + 'id.txt'
	SCORE_FILE = RES + 'score.json'
	BACKGROUND_FILE = RES + 'background.png'

class Performance:
	IMAGE_WIDTH = 160
	IMAGE_HEIGHT = 160

class Color:
	BLACK = (0, 0, 0)
	RED = (255, 0, 0)
	GREEN = (0, 255, 0)
	BLUE = (0, 0, 255)
	WHITE = (255, 255, 255)

	WIREFRAME_COLORS = [ 
		(255, 0, 0), (255, 85, 0), (255, 170, 0),
		(255, 255, 0), (170, 255, 0), (85, 255, 0),
		(0, 255, 0), (0, 255, 85), (0, 255, 170),
		(0, 255, 255), (0, 170, 255), (0, 85, 255),
		(0, 0, 255), (85, 0, 255), (170, 0, 255),
		(255, 0, 255), (255, 0, 170), (255, 0, 85)
	]