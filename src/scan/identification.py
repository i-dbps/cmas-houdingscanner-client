from constants import Path

class Identification():

	def __init__(self):
		self.id = None

		self.load()

	def load(self):
		try:
			file = open(Path.ID_FILE, 'r')
			self.id = file.read()
			file.close()
		except IOError:
			pass

	def save(self):
		file = open(Path.ID_FILE, 'w')
		file.write(self.id)

	def is_identified(self):
		return self.id != None