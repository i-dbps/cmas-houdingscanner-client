from constants import Path

class Scan:

	def __init__(self, exercises):
		self.exercises = exercises

	def is_completed(self):
		for exercise in self.exercises:
			if not exercise.completed:
				return False

		return True

	def export(self):
		json = '{'
		json += '\"results\":['
		for exercise in self.exercises:
			json += '{'
			json += '\"name\":\"' + exercise.pose.get_name() + '\",'
			json += '\"score\":' + str(exercise.score) + ','
			json += '\"time\":' + str(exercise.time)
			json += '},'

		json = json[:-1]
		json += ']'
		json += '}'

		try:
			file = open(Path.SCORE_FILE, 'w')
			file.write(json)
		except IOError:
			pass