import cv2

from constants import *

class Camera:

	def __init__(self):
		self.camera = cv2.VideoCapture(0)

		# validate camera
		result, frame = self.read_frame()
		if not result:
			raise RuntimeError('[Camera] - Camera not detected')

	def read_frame(self):
		self.last_frame = self.camera.read()
		return self.last_frame

	def get_last_frame(self):
		return self.last_frame

	def dispose(self):
		self.camera.release()