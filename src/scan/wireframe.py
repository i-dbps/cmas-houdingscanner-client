import cv2
import time
import math
import threading
import numpy as np

from constants import *

class Wireframe():

    def __init__(self):
        self.net = cv2.dnn.readNetFromCaffe(Path.PROTO_FILE, Path.WEIGHTS_FILE)
        #self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
        self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL)

        self.points = []
        for i in range(PointId.POINT_COUNT):
            self.points.append(None)

        self.running = False

    def start(self, camera):
        if not self.running:
            thread = threading.Thread(target=self._start, args=(camera, ))
            thread.daemon = True
            thread.start()

    def stop(self):
        self.running = False

    def _start(self, camera):
        self.running = True
        while self.running:
            _, frame = camera.get_last_frame()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = cv2.UMat(cv2.resize(frame, (Performance.IMAGE_WIDTH, Performance.IMAGE_HEIGHT)))

            start_time = time.time()
            count = self.update(frame)
            print ('[Wireframe] - Calculating points took ', (time.time() - start_time) * 1000, '. Found ', count, ' points')

    def update(self, frame, width=Performance.IMAGE_WIDTH, height=Performance.IMAGE_HEIGHT):
        self.net.setInput(cv2.dnn.blobFromImage(frame, 1.0 / 255, (width, height), (0, 0, 0)))
        result = self.net.forward()

        vertical_blocks = result.shape[2]
        horizontal_blocks = result.shape[3]

        count = 0
        for i in range(PointId.POINT_COUNT):
            _, probability, _, point = cv2.minMaxLoc(result[0, i, :, :])
            if probability > 0.075:
                x = int((width * point[0]) / horizontal_blocks)
                y = int((height * point[1]) / vertical_blocks)
                self.points[i] = (x, y)
                count += 1
            else:
                self.points[i] = None

        return count

    def debug(self, frame):
        for pair in PointId.LINKS:
            a = pair[0]
            b = pair[1]

            if self.points[a] != None and self.points[b] != None:
                meanX = (self.points[a][0] + self.points[b][0]) / 2
                meanY = (self.points[a][1] + self.points[b][1]) / 2
                dx = self.points[a][0] - self.points[b][0]
                dy = self.points[a][1] - self.points[b][1]
                length = math.sqrt(dx * dx + dy * dy)
                angle = math.atan2(dy, dx) * 180 / np.pi
                poly = cv2.ellipse2Poly((int(meanX), int(meanY)), (int(length / 2), 4), int(angle), 0, 360, 1)
                cv2.fillConvexPoly(frame, poly, Color.WIREFRAME_COLORS[b], cv2.LINE_AA)
