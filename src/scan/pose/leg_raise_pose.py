from .pose import Pose
from constants import PointId

class LegRaisePose(Pose):

    def __init__(self):
        pass

    def get_name(self):
        return 'Been omhoog houden'

    def get_image_name(self):
    	return 'pose_leg_raise.png'

    def get_info(self):
        return 'Bij deze oefening is het de bedoeling dat je je been zo lang mogelijk gestrekt omhoog kan houden.'

    def is_in_pose(self, wireframe):
        right_foot = wireframe.points[PointId.RIGHT_FOOT]
        right_knee = wireframe.points[PointId.RIGHT_KNEE]

        if right_foot is None or right_knee is None:
            return False

        return right_foot[1] < right_knee[1] * 1.25

    def calculate_score(self, time):
        score = 0
        if time == 0:
            score = 0
        elif time <= 9:
            score = 1
        elif time <= 29:
            score = 2
        elif time <= 59:
            score = 3
        elif time <= 119:
            score = 4
        else:
            score = 5

        return score