from .pose import Pose
from constants import PointId

class HandsUpPose(Pose):

    def __init__(self):
        pass

    def get_name(self):
        return 'Handen omhoog houden'

    def get_image_name(self):
    	return 'pose_hands_up.png'

    def get_info(self):
        return 'Bij deze oefening is het de bedoeling dat je je handen zo lang mogelijk gestrekt boven je hoofd kan houden.'

    def is_in_pose(self, wireframe):
        face = wireframe.points[PointId.FACE]
        left_hand = wireframe.points[PointId.LEFT_HAND]
        right_hand = wireframe.points[PointId.RIGHT_HAND]

        if face is None or left_hand is None or right_hand is None:
            return False

        return left_hand[1] < face[1] and right_hand[1] < face[1]

    def calculate_score(self, time):
        score = 0
        if time == 0:
            score = 0
        elif time <= 9:
            score = 1
        elif time <= 29:
            score = 2
        elif time <= 59:
            score = 3
        else:
            score = 4

        return score