import sys

from PySide2.QtWidgets import QApplication
from main import Main

if __name__ == '__main__':
    app = QApplication(sys.argv)

    Main()

    sys.exit(app.exec_())
 