# Dit programma is in opdracht van Roel Bindels gemaakt voor het wilhelmina ziekenhuis
# Auteurs: Rowan Koeman, Patrick Wenzkowski, Tim Smeets, Aalt Vos en Eva Vrosch

import os

import cv2
import time
import threading

import numpy as np

from constants import *

from scan.scan import Scan
from scan.exercise import Exercise
from scan.identification import Identification

from ui import *
from scan.pose import *

from PySide2.QtWidgets import QApplication, QMainWindow, QStackedWidget

class Main(QMainWindow):

	def __init__(self):
		super().__init__()

		self.identification = Identification()
		self.scan = Scan([
			Exercise(HandsUpPose()),
			Exercise(LegRaisePose())
		])

		self.scan.exercises[1].completed = True
		self.scan.exercises[1].score = 5

		self.setWindowTitle('CMAS')
		self.setGeometry(0, 0, UI.WIDTH, UI.HEIGHT)

		self.central_widget = QStackedWidget()
		self.setCentralWidget(self.central_widget)

		self.interface = None
		self.set_interface(StartInterface(self))

		self.setStyleSheet(open('../res/css/background.css', 'r').read())

		self.show()

		thread = threading.Thread(target=self.run)
		thread.daemon = True
		thread.start()

	def run(self):
		running = True
		while running:
			time.sleep(0.1)
			if self.interface != None:
				self.interface.update()

	def set_interface(self, interface):
		if self.interface != None:
			self.interface.dispose()
			self.central_widget.removeWidget(self.interface)

		self.interface = interface
		self.central_widget.addWidget(interface)
		self.central_widget.setCurrentWidget(interface)