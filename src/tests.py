import cv2
import numpy as np

from scan.camera import Camera
from constants import *

def test_pose(pose, wireframe):
	camera = Camera()
	wireframe.start(camera)

	while True:
		_, frame = camera.read_frame()

		wireframe.debug(frame)
		in_pose = pose.is_in_pose(wireframe)
		#print ('we are', in_pose, ' in pose')

		cv2.imshow("CMAS Debug", frame)

		if cv2.waitKey(1) == 27: 
			break

	cv2.destroyAllWindows()